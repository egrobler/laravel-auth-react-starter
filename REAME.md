# Setup / Installation
## Install Laravel
Follow the instructions here: https://laravel.com/docs/5.7/installation - or -

- In the command line 
```console 
~:$ composer global require "laravel/installer"
```
(Note: Scaffold a new Laravel project in the folder of your choosing)

```console
~:$ laravel new starter-app
~:$ cd starter-app
starter-app/:$ php artisan serve
```
At this stage you can visit http://127.0.0.1:8000/ and you should see the Laravel app homepage, meaning everything is OK.

(Note: Let's setup authentication)
1. First we need to setup a database connection. We will be using sqlite since this is easiest to configure for demo purposes.
You can follow the instructions here: https://laravel.com/docs/5.7/database - or -

1.a)
```console
starter-app/:$ touch database/database.sqlite
```
1.b)
Replace the DB relate key/value pairs in your .env file
Replace */absolute/path/to/database.sqlite* with the path to your database.sqlite file created in step 1.a above

```sh
# DB_CONNECTION=mysql
# DB_HOST=127.0.0.1
# DB_PORT=3306
# DB_DATABASE=homestead
# DB_USERNAME=homestead
# DB_PASSWORD=secret
DB_CONNECTION=sqlite
DB_DATABASE=/absolute/path/to/database.sqlite
```

2. Scaffold out the auth routes and views
```console
starter-app/:$ php artisan make:auth
```

If you visit http://127.0.0.1:8000/ now, you'll see that a Login and Register link is showing at the top right of the page.

Also, if you try and register now, you'll get some kind of a SQL error. This is because we need to create the tables used for auth first. Luckily Laravel already catered for that. We only need to run:

```console
starter-app/:$ php artisan migrate
```

Now visit http://127.0.0.1:8000/ again and re-register (or just refresh the error page), and things should work fine. You can also now login with that same user.

2.a) For the fun of it, let's use passport authentication instead of the built-in token authentication: https://laravel.com/docs/5.7/passport - or -

```console
starter-app/:$ composer require laravel/passport
starter-app/:$ php artisan migrate
starter-app/:$ php artisan passport:install
```
- Update your **App\User** model accordingly
    ```php
    <?php

    namespace App;

    /* Add this line */
    use Laravel\Passport\HasApiTokens;
    
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Foundation\Auth\User as Authenticatable;

    class User extends Authenticatable
    {
        /* Add HasApiTokens here */
        use HasApiTokens, Notifiable;
    }
    ```
- Update your **AuthServiceProvider** like this:
(path: app/Providers/AuthServiceProvider.php )

    ```php
    <?php

    namespace App\Providers;

    /* Add this line */
    use Laravel\Passport\Passport;
    
    use Illuminate\Support\Facades\Gate;
    use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

    class AuthServiceProvider extends ServiceProvider
    {
        /**
         * The policy mappings for the application.
         *
         * @var array
        */
        protected $policies = [
            'App\Model' => 'App\Policies\ModelPolicy',
        ];

        /**
         * Register any authentication / authorization services.
         *
         * @return void
        */
        public function boot()
        {
            $this->registerPolicies();
            
            /* Add this line */
            Passport::routes();
        }
    }
    ```
- Update your config/auth.php file to use the passport driver instead of the token driver:
    ```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'passport',
            'provider' => 'users',
        ],
    ],
    ```
- In your config/app.php, add `Laravel\Passport\PassportServiceProvider::class,` as a service provider:
    ```php
    <?php
    //...

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        //...

        Laravel\Passport\PassportServiceProvider::class,
    ],

    //...
    ```

3. Now we can test the passport api out by using some Vue components, which is also available by running the following command:

```console
starter-app/:$ php artisan vendor:publish --tag=passport-components
```

3.a) At this stage, we'd need to compile all js assets
First off if you haven't done this, now's a good time to run npm install inside your main projec folder, to install all necessary node modules, etc. etc.
```console
starter-app/:$ npm install
starter-app/:$ npm run dev
```
3.b) Now just add the following lines to your resources/js/app.js file:

```js
Vue.component('example-component', require('./components/ExampleComponent.vue'));

/** Add these 3 lines **/
Vue.component("passport-clients", require("../assets/js/components/passport/Clients.vue"));
Vue.component("passport-authorized-clients", require("../assets/js/components/passport/AuthorizedClients.vue"));
Vue.component("passport-personal-access-tokens", require("../assets/js/components/passport/PersonalAccessTokens.vue"));
/** ..... **/

const app = new Vue({
    el: '#app'
});
```
3.c) Then just to test it out, add the following below the text "You are logged in!" in your resources/views/home.blade.php file

```blade
    <passport-clients></passport-clients>
    <passport-authorized-clients></passport-authorized-clients>
    <passport-personal-access-tokens></passport-personal-access-tokens>
```

3.d) Run 
```console
starter-app/:$ php artisan serve
```
again, login, and then you'll see 2 boxes (OAuth Clients and Personal Access Tokens). Now you've implemented your own passport server and functionality!

3.e) So what about React?
Run ```console
starter-app/:$ php artisan preset react
starter-app/:$ npm install
starter-app/:$ npm run dev
```
